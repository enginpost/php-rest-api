<?php
		include_once( './lib/PHPRest/Request.class.php' );

		$objRequest = new PHPRest\Request();
		$controller_name = ucfirst( $objRequest->url_elements[1] . 'Controller' );

		if( class_exists( $controller_name ) ){
			$objController = new $controller_name();
			$action_name = strtolower( $objRequest->verb ) . 'Action';
			$result = $objController->$action_name( $objRequest );
			$view_name = ucfirst( $objRequest->format ) . 'View';
			if( class_exists( $view_name ) ){
				$view = new $view_name();
				$view->render( $result );
			}
		}