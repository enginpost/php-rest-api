<?php

namespace PHPRest\Abstracts;

use PHPRest\Interfaces as Interfaces;

abstract class ControllerAbstract implements Interfaces\ControllerInterface{
	public function getAction( $request ){
		$data = array();
		$data['error'] = 'This API controller does not implement the get action.';
		return $data;
	}
	public function postAction( $request ){
		$data = array();
		$data['error'] = 'This API controller does not implement the post action.';
		return $data;
	}
	public function putAction( $request ){
		$data = array();
		$data['error'] = 'This API controller does not implement the put action.';
		return $data;
	}
	public function deleteAction( $request ){
		$data = array();
		$data['error'] = 'This API controller does not implement the delete action.';
		return $data;
	}
}
