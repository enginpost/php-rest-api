<?php

namespace PHPRest\Abstracts;

use PHPRest\Interfaces as Interfaces;

abstract class ModelAbstract implements Interfaces\ModelInterface{
	public function createData( $request ){
		$data = array();
		$data['error'] = 'This API model has not implemented the create data method.';
		return $data;
	}
	public function readData( $request ){
		$data = array();
		$data['error'] = 'This API model has not implemented the read data method.';
		return $data;
	}
	public function updateData( $request ){
		$data = array();
		$data['error'] = 'This API model has not implemented the update data method.';
		return $data;
	}
	public function deleteData( $request ){
		$data = array();
		$data['error'] = 'This API model has not implemented the delete data method.';
		return $data;
	}
}
