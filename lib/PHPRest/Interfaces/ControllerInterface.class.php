<?php

namespace PHPRest\Interfaces;

interface ControllerInterface{
	public function getAction( $request );		// Gets an existing resources (zero or more)
	public function postAction( $request );		// Saves new resources
	public function putAction( $request );		// Updates existing resources
	public function deleteAction( $request );	// Deletes an existing resource
}
