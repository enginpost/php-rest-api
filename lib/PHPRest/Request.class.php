<?php

namespace PHPRest;

class Request {

	private $_url_elements;
	private $_verb;
	private $_parameters;
	private $_format;

	public function __construct(){

		spl_autoload_register( 'PHPRest\Request::apiAutoload' );

		$this->_verb = $_SERVER[ 'REQUEST_METHOD' ];
		$this->_url_elements = explode( '/', $_SERVER[ 'PATH_INFO' ] );

		$this->_parseIncomingParams();
		$this->_format = 'json'; //JSON is the default format
		if( isset( $this->_parameters[ 'format' ] ) ){
			$this->_format = $this->_parameters[ 'format' ];
		}

		return true;

	}

	public function __get( $this_property ){

		$property = '_' . $this_property;

		if( property_exists( $this, $property ) ){
			return $this->{$property};
		}

	}

	public function __set( $this_property, $this_value ){

		$property = '_' . $this_property;

		if( property_exists( $this, $property ) ){
			$this->{$property} = $this_value;
		}

	}

	public function apiAutoload( $this_classname ){

		$class_path = "";

		if( preg_match( '/[a-zA-Z]+Interface$/', $this_classname ) ){
			$class_path = './lib/PHPRest/Interfaces/' . substr( $this_classname, strrpos( $this_classname, '\\' ) + 1 ) . '.class.php';
		}elseif( preg_match( '/[a-zA-Z]+Abstract$/', $this_classname ) ){
			$class_path = './lib/PHPRest/Abstracts/' . substr( $this_classname, strrpos( $this_classname, '\\' ) + 1 ) . '.class.php';
		}elseif( preg_match( '/[a-zA-Z]+Controller$/', $this_classname ) || preg_match( '/[a-zA-Z]+ControllerAbstract$/', $this_classname ) ){
			//dynamically load Controller classes
			$class_path =  './controllers/' . $this_classname . '.class.php';
		}elseif ( preg_match( '/[a-zA-Z]+Model$/', $this_classname ) || preg_match( '/[a-zA-Z]+ModelAbstract$/', $this_classname ) ) {
			// dynamically load Model classes
			$class_path =  './models/' . $this_classname . '.class.php';
		}elseif ( preg_match( '/[a-zA-Z]+View$/', $this_classname ) || preg_match( '/[a-zA-Z]+ViewAbstract$/', $this_classname ) ) {
			# dynamically load View classes
			$class_path =  './views/' . $this_classname . '.class.php';
		}

		include $class_path;
	}

	private function _parseIncomingParams(){

		$these_parameters = array();

		// GET METHOD first
		if( isset( $_SERVER[ 'QUERY_STRING'] ) ){
			parse_str( $_SERVER[ 'QUERY_STRING'], $these_parameters );
		}

		// then PUT or POST
		$this_data = file_get_contents('php://input');
		$this_content_type = false;
		if( isset( $_SERVER[ 'CONTENT_TYPE' ] ) ){
			$this_content_type = $_SERVER[ 'CONTENT_TYPE' ];
		}
		switch( $this_content_type ){

			case 'application/json' :

				$data_params = json_decode( $this_data );
				if( $data_params ){
					foreach( $data_params as $param_id => $param_val ){
						$these_parameters[ $param_id ] = $param_val;
					}
				}
				$this->_format = 'json';
				break;

			case 'application/x-www-form-urlencoded':

				parse_str( $this_data, $post_vars );
				foreach( $post_vars as $post_var => $var_val ){
					$these_parameters[ $post_var ] = $var_val;
				}
				$this->_format = 'html';
				break;

			default:

				//more formats here

		}

		$this->_parameters = $these_parameters;

	}

}
