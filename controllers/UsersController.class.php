<?php

class UsersController extends PHPRest\Abstracts\ControllerAbstract{

	public function getAction( $request ){
		$data = array();
		$url_elements = $request->url_elements;
		if( isset( $url_elements[2] ) ){
			$user_id = (int)$url_elements[2];
			if( isset( $url_elements[3] ) ){
				switch( $url_elements[3] ){
					case 'friends' :
						$data['message'] = "Here is info about user " . $user_id . "'s many friends.";
						break;
					default :
						//there is no default
						break;
				}
			}else{
				$data['message'] = 'Here is the info for user ' . $user_id . '.';
			}
		}else{
			$data['message'] = 'You want a list of users';
		}
		return $data;
	}

	public function postAction( $request ){
		$data = $request->parameters;
		$data['message'] = "This data was submitted.";
		return $data;
	}

}
